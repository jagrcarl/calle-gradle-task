package se.experis;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.nio.file.NoSuchFileException;
import java.util.Scanner;


public class Program {
    public static void main(String[] args) {
        try {
            //finds the .jsonfile
            InputStream reader = Thread.currentThread().getContextClassLoader().getResourceAsStream("people.json");

            //create ObjectMapper instance
            ObjectMapper objectMapper = new ObjectMapper();

            //read customer.json file into tree model
            JsonNode parser = objectMapper.readTree(reader);

            Scanner scan = new Scanner(System.in);
            System.out.println("Type what you want to search for: "); //lets the user type in
            String ui = scan.nextLine(); //stores the iu

            for (JsonNode pm : parser.path("person")) { //if the ui exists in the json-file, it will write it here
                if (pm.path("name").textValue().contains(ui) || pm.path("address").textValue().contains(ui) || pm.path("mobileNr").textValue().contains(ui) || pm.path("workNr").textValue().contains(ui) || pm.path("dateOfBirth").textValue().contains(ui)) {
                    System.out.println(pm.path("name").asText() + ", " + pm.path("address").asText() + ", " + pm.path("mobileNr").asText() + ", " + pm.path("workNr").asText() + ", " + pm.path("dateOfBirth").asText());
                }
            }

            reader.close();

        } catch (NoSuchFileException ex) {
            System.out.println("The files is missing");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
